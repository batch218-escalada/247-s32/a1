const http = require('http');
const port = 4000

const server = http.createServer((req, res) => {

    // 1. http://localhost:4000/
    if (req.url == '/' && req.method == 'GET') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to Booking System.');
    }

    // 2. http://localhost:4000/profile
    if (req.url == '/profile' && req.method == 'GET') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to your profile.');
    }

    // 3. http://localhost:4000/courses
    if (req.url == '/courses' && req.method == 'GET') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Here\'s our courses available.');
    }

    // 4. http://localhost:4000/addcourse
    if (req.url == '/addcourse' && req.method == 'POST') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Add a course to our resources.');
    }

    // 5. http://localhost:4000/updatecourse
    if (req.url == '/updateCourse' && req.method == 'PUT') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Update a course to a database.');
    }

    // 6. http://localhost:4000/archivecourses
    if (req.url == '/archivecourses' && req.method == 'DELETE') {

        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Archive courses to our resources.');
    }

});

server.listen(port);

console.log(`Listening on port ${port}`);

